//
//  UserTransactionsWebCall.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/7/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import AddictiveLib
import Unbox
import Alamofire

class UserTransactionsWebCall: WebCall<[UserTransaction]> {
	
	init(_ name: String) {
		super.init(call: "investor/\(name)/investments")
	}
	
	override func convertData(_ data: Data) -> [UserTransaction]? {
		guard let json = try? JSONSerialization.jsonObject(with: data) else {
			return nil
		}
		
		if let list = json as? [Any] {
			var newDictionary: [String: Any] = [:]
			newDictionary["list"] = list
			
			guard let payload = try? UserTransactionsPayload(unboxer: Unboxer(dictionary: newDictionary)) else {
				return nil
			}
			
			var list: [UserTransaction] = []
			for transaction in payload.transactions {
				list.append(UserTransaction(transaction.id, postId: transaction.post, responseId: transaction.response, time: Date.init(timeIntervalSince1970: TimeInterval(transaction.time)), amount: transaction.amount, upvotes: transaction.initialUpvotes, profit: transaction.profit, finished: transaction.finished, success: transaction.success))
			}
			return list
		}
		
		return nil
	}
	
}

class UserTransactionsPayload: WebCallPayload {
	
	let transactions: [IndividualTransactionPayload]
	
	required init(unboxer: Unboxer) throws {		
		self.transactions = try unboxer.unbox(key: "list", allowInvalidElements: false)
	}
	
}

class IndividualTransactionPayload: WebCallPayload {

	let id: Int
	let amount: Int
	let finished: Bool
	let post: String
	let time: CLong
	let initialUpvotes: Int
	let profit: Int
	let success: Bool
	let response: String
	
	required init(unboxer: Unboxer) throws {
		self.id = try unboxer.unbox(key: "id")
		self.amount = try unboxer.unbox(key: "amount")
		self.finished = try unboxer.unbox(key: "done")
		self.post = try unboxer.unbox(key: "post")
		self.time = try unboxer.unbox(key: "time")
		self.initialUpvotes = try unboxer.unbox(key: "upvotes")
		self.profit = try unboxer.unbox(key: "profit")
		self.success = try unboxer.unbox(key: "success")
		self.response = try unboxer.unbox(key: "response")
	}
	
}

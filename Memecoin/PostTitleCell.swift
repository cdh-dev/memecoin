//
//  PostTitleCell.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/12/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import AddictiveLib
import UIKit

class PostTitleCell: TableCell {
	
	let title: String
	
	init(title: String) {
		self.title = title
		
		super.init("post-title")
		
		self.setCallback() {
			template, cell in
			
			guard let cell = cell as? UIPostTitleCell else {
				return
			}
			
			cell.titleLabel.text = self.title
		}
		
		self.setSelectionStyle(.none)
	}
	
}

class UIPostTitleCell: UITableViewCell {
	
	@IBOutlet weak var titleLabel: UILabel!
	
}

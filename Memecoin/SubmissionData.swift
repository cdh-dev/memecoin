//
//  SubmissionData.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/10/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import AddictiveLib

class SubmissionData {
	
	let title: String
	let author: String

	let birthday: Date

	let upvotes: Int
	let score: Int
	
	let link: URL
	let thumbnail: URL?
	
	let responseLink: URL
	let responseBody: String
	let responseBirthday: Date
	
	var image: UIImage?
	
	init(title: String, author: String, birthday: Date, upvotes: Int, score: Int, link: URL, thumbnail: URL?, reponseLink: URL, reponseBody: String, responseBirthday: Date) {
		self.title = title
		self.author = author

		self.birthday = birthday

		self.upvotes = upvotes
		self.score = score

		self.link = link
		self.thumbnail = thumbnail

		self.responseLink = reponseLink
		self.responseBody = reponseBody
		self.responseBirthday = responseBirthday
	}
	
}



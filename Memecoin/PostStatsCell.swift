//
//  PostStatsCell.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/12/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import AddictiveLib
import UIKit

class PostStatsCell: TableCell {
	
	let buyin: Int
	let upvotes: Int
	let approval: Float
	
	init(buyin: Int, upvotes: Int, approval: Float) {
		self.buyin = buyin
		self.upvotes = upvotes
		self.approval = approval
		
		super.init("post-stats")
		
		self.setSelectionStyle(.none)
		self.setCallback() {
			template, cell in
			
			guard let cell = cell as? UIPostStatsCell else {
				return
			}
			
			let positive = "FF6C00"
			let negative = "6DA1FF"
			
			if self.buyin >= 0 {
				cell.buyinLabel.textColor = UIColor(hex: positive)
				cell.buyinLabel.text = "\(self.buyin)↑"
			} else {
				cell.buyinLabel.textColor = UIColor(hex: negative)
				cell.buyinLabel.text = "\(self.buyin)↓"
			}
			
			if self.upvotes >= 0 {
				cell.upvotesLabel.textColor = UIColor(hex: positive)
				cell.upvotesLabel.text = "\(self.upvotes)↑"
			} else {
				cell.upvotesLabel.textColor = UIColor(hex: negative)
				cell.upvotesLabel.text = "\(self.upvotes)↓"
			}
		}
	}
	
}

class UIPostStatsCell: UITableViewCell {
	
	@IBOutlet weak var buyinLabel: UILabel!
	@IBOutlet weak var upvotesLabel: UILabel!
	@IBOutlet weak var approvalLabel: UILabel!
	
}

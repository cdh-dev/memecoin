//
//  MainDetailViewController.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/11/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import UIKit
import AddictiveLib
import Presentr

class MainDetailViewController: TableHandler {
	
	var investment: UserTransaction!
	var redditData: SubmissionData?
	
	@IBOutlet weak var tableReference: UITableView!
	
	@IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
	
	@IBOutlet weak var initialInvestmentLabel: UILabel!
	@IBOutlet weak var predictedReturnLabel: UILabel!
	@IBOutlet weak var predictedReturnTagLabel: UILabel!
	
	@IBOutlet weak var predictedReturnStack: UIStackView!
	@IBOutlet weak var loadingReturnView: UIView!
	
	private let imagePresentr: Presentr = {
		let presentr = Presentr(presentationType: .popup)
		
		presentr.blurBackground = true
		presentr.dismissOnSwipe = true
		
		presentr.transitionType = TransitionType.crossDissolve
		presentr.dismissTransitionType = TransitionType.crossDissolve
		
		presentr.roundCorners = false
		
		return presentr
	}()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.link(self.tableReference)
		
		self.initialInvestmentLabel.text = "\(self.investment.amount)"
	}
	
	private func setupRefresh() {
		self.tableView.refreshControl = UIRefreshControl()
		self.tableView.refreshControl?.addTarget(self, action: #selector(MainDetailViewController.pullFetchData), for: .valueChanged)
		self.tableView.refreshControl?.layer.zPosition = -1
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.loadingIndicator.isHidden = false
		self.loadingIndicator.startAnimating()
		
		self.fetchData(force: false) { // Fetch table data
			self.loadingIndicator.stopAnimating()
			
			self.reloadTable()
			self.reloadResultView()

			self.setupRefresh()
		}
		
		self.reloadResultView()
	}
	
	override func refresh() {
		if self.redditData == nil {
			return
		}
		
		let section = self.tableForm.addSection()
		
		if self.investment.finished {
			section.addCell(self.investment.success ? "success" : "failure").setSelectionStyle(.none)
		}
		
		section.addSpacerCell().setBackgroundColor(.clear).setHeight(10)
		section.addCell(PostTitleCell(title: "\"\(self.redditData!.title)\""))
		section.addSpacerCell().setBackgroundColor(.clear).setHeight(10)

		if let image = self.redditData!.image {
			section.addCell(ImageCell(image)).setHeight(200).setSelectionStyle(.none)
			section.addSpacerCell().setBackgroundColor(.clear).setHeight(15)
		}
		
		let age = TimeUtils.timeToDate(to: self.redditData!.birthday)
		section.addCell(PostMetaCell(name: self.redditData!.author, birthday: "\(TimeUtils.timeToString(years: age.years, days: age.days, hours: age.hours, min: age.minutes, sec: age.seconds)) ago"))
		section.addSpacerCell().setBackgroundColor(.clear).setHeight(15)

		section.addDividerCell(left: 30, right: 30, backgroundColor: UIColor.clear, insetColor: UIColor(hex: "2C2C2C")!)

		section.addSpacerCell().setBackgroundColor(.clear).setHeight(15)
		section.addCell(PostStatsCell(buyin: self.investment.upvotes, upvotes: self.redditData!.upvotes, approval: 0.0))
		section.addSpacerCell().setBackgroundColor(.clear).setHeight(20)
		
		if !self.investment.finished {
			section.addCell("progress").setSelectionStyle(.none)
		}
	}
	
	@objc private func pullFetchData() {
		HapticUtils.SELECTION.selectionChanged()
		self.fetchData(force: true) {
			if self.redditData == nil {
				HapticUtils.IMPACT.impactOccurred()
			} else {
				HapticUtils.SELECTION.selectionChanged()
			}
			
			self.tableView.refreshControl?.endRefreshing()
			
			self.reloadTable()
			self.reloadResultView()
		}
	}
	
	private func reloadResultView() {
		self.predictedReturnStack.isHidden = true
		self.loadingReturnView.isHidden = false

		if self.redditData == nil {
			self.predictedReturnStack.isHidden = true
			self.loadingReturnView.isHidden = false
			return
		}
		
		if self.investment.finished {
			self.predictedReturnStack.isHidden = false
			self.loadingReturnView.isHidden = true
			
			self.predictedReturnTagLabel.text = self.investment.profit > 0 ? "Profit" : "Loss"
			
			if self.investment.profit > 0 {
				self.predictedReturnLabel.textColor = UIColor(hex: "FFAC27")
				self.predictedReturnLabel.text = "+\(self.investment.profit)"
			} else {
				self.predictedReturnLabel.textColor = UIColor(hex: "F32323")
				self.predictedReturnLabel.text = "\(self.investment.profit)"
			}
			return
		}
		
		self.fetchPredictedReturn() {
			result in
			
			self.predictedReturnStack.isHidden = false
			self.loadingReturnView.isHidden = true
			
			self.predictedReturnTagLabel.text = "Predicted Return"

			if result == nil || result! > 0 {
				self.predictedReturnLabel.textColor = UIColor(hex: "FFAC27")
				self.predictedReturnLabel.text = result == nil ? "ERR" : "+\(result!)"
			} else {
				self.predictedReturnLabel.textColor = UIColor(hex: "F32323")
				self.predictedReturnLabel.text = "\(result!)"
			}
		}
	}
	
	private func fetchData(force: Bool, completed: @escaping () -> Void = {}) {
		RedditManager.instance.fetchData(self.investment, force: force) {
			data in
			
			self.redditData = data
			
			if self.redditData != nil && self.redditData!.thumbnail != nil {
				DownloadImageCall(url: self.redditData!.thumbnail!).callback() {
					error, image in
					
					if self.redditData != nil {
						self.redditData!.image = image
					}
					
					completed()
				}.execute()
			}
		}
	}
	
	private func fetchPredictedReturn(completed: @escaping (Int?) -> Void) {
		PredictResultWebCall(initial: self.investment.upvotes, current: self.redditData!.score).callback() {
			error, data in
			
			guard let multiplier = data else {
				completed(nil)
				return
			}
			
			let result: Int = Int(Double(self.investment.amount) * multiplier)
			let prediction = result - self.investment.amount
			
			completed(prediction)
			return
		}.execute()
	}
	
	@IBAction func doneButtonClicked(_ sender: Any) {
		HapticUtils.SELECTION.selectionChanged()
		
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func openPostButtonClicked(_ sender: Any) {
		HapticUtils.SELECTION.selectionChanged()
		
		if self.redditData != nil {
			UIApplication.shared.open(self.redditData!.link, options: [:], completionHandler: nil)
		}
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		super.tableView(tableView, didSelectRowAt: indexPath)
		
		guard let section = self.tableForm.getSection(indexPath), let cell = section.getCell(indexPath) else {
			return
		}
		
		if cell is ImageCell {
			if let reddit = self.redditData, let image = reddit.image {
				HapticUtils.IMPACT.impactOccurred()

				self.customPresentViewController(self.imagePresentr, viewController: {
					let controller = self.storyboard?.instantiateViewController(withIdentifier: "main-detail-image-view") as! MainDetailImageViewController
					controller.image = image
					return controller
				}(), animated: true)
			}
		}
	}
}

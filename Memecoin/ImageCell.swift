//
//  ImageCell.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/11/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import AddictiveLib
import UIKit

class ImageCell: TableCell {
	
	var image: UIImage?
	
	init(_ image: UIImage?) {
		self.image = image
		
		super.init("post-image")
		
		self.setCallback() {
			form, cell in
			
			guard let imageCell = cell as? UIImageCell else {
				return
			}
			
			imageCell.postImageView.image = self.image
		}
	}
	
}

class UIImageCell: UITableViewCell {
	
	@IBOutlet weak var postImageView: UIImageView!
	
}

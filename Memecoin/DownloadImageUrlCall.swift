//
//  DownloadImageCall.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/12/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import AddictiveLib
import Unbox
import UIKit

class DownloadImageUrlCall: UnboxWebCall<ImageURLPayload, URL> {
	
	init(name: String) {
		super.init(url: "https://www.reddit.com/", call: "user/\(name)/about.json")
	}
	
	override func convertToken(_ data: ImageURLPayload) -> URL? {
		guard let string = data.profilePicUrl else {
			return nil
		}
		return URL(string: string)
	}
	
}

class ImageURLPayload: WebCallPayload {
	
	let profilePicUrl: String?
	
	required init(unboxer: Unboxer) throws {
		self.profilePicUrl = unboxer.unbox(keyPath: "data.icon_img")
	}
	
}

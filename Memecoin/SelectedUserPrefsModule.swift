//
//  SelectedUserPrefsModule.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/7/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import AddictiveLib

class SelectedUserPrefsModule: StorageHandler {
	
	var storageKey: String = "mainUser"
	
	private(set) var mainUser: InvestmentManager.Username? // The main data here
	
	func userChangedUserValue(_ username: InvestmentManager.Username) {
		let username = username.replacingOccurrences(of: " ", with: "")
		
		self.mainUser = username
		InvestmentManager.instance.saveStorage() // Save self
	}
	
	func saveData() -> Any? {
		return self.mainUser
	}
	
	func loadData(data: Any) {
		if let val = data as? InvestmentManager.Username {
			self.mainUser = val
		}
	}
	
	func loadDefaults() {

	}
	
}

//
//  UserData.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/7/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import UIKit

class UserData {
	
	let name: String
	var image: UIImage?

	var balance: Int64
	var broke: Int
	
	var transactions: [UserTransaction]
	
	init(_ name: String, image: UIImage?, balance: Int64, broke: Int, transactions: [UserTransaction] = []) {
		self.name = name
		self.image = image
		
		self.balance = balance
		self.broke = broke
		
		self.transactions = transactions
	}
	
}

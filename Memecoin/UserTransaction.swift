//
//  UserTransaction.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/7/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation

class UserTransaction {
	
	let id: InvestmentManager.InvestmentID
	let postId: String
	let responseId: String
	
	let time: Date

	let amount: Int
	let upvotes: Int
	
	let profit: Int
	
	let finished: Bool
	let success: Bool

	init(_ id: InvestmentManager.InvestmentID, postId: String, responseId: String, time: Date, amount: Int, upvotes: Int, profit: Int, finished: Bool, success: Bool) {
		self.id = id
		self.postId = postId
		self.responseId = responseId
		
		self.time = time
		
		self.amount = amount
		self.upvotes = upvotes
		
		self.profit = profit
		
		self.finished = finished
		self.success = success
	}
	
}

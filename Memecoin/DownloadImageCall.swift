//
//  DownloadImageUrlCall.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/11/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import AddictiveLib
import Alamofire

class DownloadImageCall: WebCall<UIImage> {
	
	init(url: URL) {
		super.init(url: "", call: url.absoluteString)
	}
	
	override func convertData(_ data: Data) -> UIImage? {
		return UIImage(data: data)
	}
	
}

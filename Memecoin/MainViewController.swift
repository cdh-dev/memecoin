//
//  MainViewController.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/6/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import AddictiveLib
import UIKit
import Presentr

class MainViewController: TableHandler {
	
	@IBOutlet weak var tableReference: UITableView!
	@IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
	
	private var loading = false
	
	private var detailPresentr: Presentr = {
		let presentr = Presentr(presentationType: .popup)
		
		presentr.cornerRadius = 8.0
		presentr.transitionType = TransitionType.coverVertical
		presentr.dismissOnSwipe = true
		
		return presentr
	}()
	
	private var popupPresentr: Presentr = {
		let presentr = Presentr(presentationType: .bottomHalf)
		
		presentr.dismissOnSwipe = true
		
		return presentr
	}()
	
	private var userData: UserData?
	
	private var timer: Timer?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.link(self.tableReference)
		
		self.tableView.estimatedRowHeight = 40
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.loadingIndicator.isHidden = false
		self.loadingIndicator.startAnimating()
		
		self.fetchData(force: false) { // Fetch table data
			self.loadingIndicator.stopAnimating()
			self.reloadTable()
			
			self.setupRefresh()
		}
	}
	
	private func startTimer() {
		self.timer = Timer.scheduledTimer(withTimeInterval: 30, repeats: true) {
			timer in
			
			self.reloadTable()
		}
	}
	
	private func setupRefresh() {
		self.tableView.refreshControl = UIRefreshControl()
		self.tableView.refreshControl?.addTarget(self, action: #selector(MainViewController.pullFetchData), for: .valueChanged)
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		
		if let timer = self.timer {
			timer.invalidate()
			self.timer = nil
		}
	}
	
	override func refresh() {
		if self.loading {
			if let timer = self.timer {
				timer.invalidate()
				self.timer = nil
			}
			
			return
		}
		
		if self.userData == nil {
			self.tableForm.addSection().addCell("noAccount").setSelectionStyle(.none)
			
			if let timer = self.timer {
				timer.invalidate()
				self.timer = nil
			}
			
			return
		}
		
		if self.timer == nil {
			self.startTimer()
		}
		
		let userData = self.userData!
		
		let headerSection = self.tableForm.addSection()
		headerSection.addCell(HeaderCell(icon: userData.image, username: userData.name, coins: userData.balance, bankruptcies: userData.broke).setHeight(250).setSelection() {
			cell, index in
			
			HapticUtils.SELECTION.selectionChanged()
			self.dropdownClicked(nil)
		})
		
		let activeSection = buildHeader(self.tableForm.addSection())
		activeSection.setTitle("ACTIVE INVESTMENTS")
		
		for investment in userData.transactions {
			if !investment.finished {
				activeSection.addDividerCell(left: 15, right: 0, backgroundColor: UIColor.clear, insetColor: UIColor(hex: "2C2C2C")!)
				self.addInvestmentCell(activeSection, investment: investment)
			}
		}
		
		if activeSection.cells.isEmpty {
			activeSection.addDividerCell(left: 15, right: 0, backgroundColor: UIColor.clear, insetColor: UIColor(hex: "2C2C2C")!)
			activeSection.addCell("noData").setHeight(40).setSelectionStyle(.none)
		} else {
			activeSection.addDividerCell(left: 15, right: 0, backgroundColor: UIColor.clear, insetColor: UIColor(hex: "2C2C2C")!)
		}
		
		activeSection.addSpacerCell().setBackgroundColor(UIColor.clear).setHeight(20)
		
		let inactiveSection = buildHeader(self.tableForm.addSection())
		inactiveSection.setTitle("PREVIOUS INVESTMENTS")
		
		for investment in userData.transactions {
			if investment.finished {
				inactiveSection.addDividerCell(left: 15, right: 0, backgroundColor: UIColor.clear, insetColor: UIColor(hex: "2C2C2C")!)
				self.addInvestmentCell(inactiveSection, investment: investment)
			}
		}
		
		if inactiveSection.cells.isEmpty {
			inactiveSection.addDividerCell(left: 15, right: 0, backgroundColor: UIColor.clear, insetColor: UIColor(hex: "2C2C2C")!)
			inactiveSection.addCell("noData").setHeight(40).setSelectionStyle(.none)
		} else {
			inactiveSection.addDividerCell(left: 15, right: 0, backgroundColor: UIColor.clear, insetColor: UIColor(hex: "2C2C2C")!)
		}
	}
	
	private func addInvestmentCell(_ section: TableSection, investment: UserTransaction) {
		section.addCell(InvestmentCell(investment: investment).setHeight(70).setSelection() {
			cell, path in
			
			HapticUtils.SELECTION.selectionChanged()
			
			self.customPresentViewController(self.detailPresentr, viewController: {
				let controller = self.storyboard?.instantiateViewController(withIdentifier: "main-detail-view") as! MainDetailViewController
				controller.investment = (cell as! InvestmentCell).investment
				return controller
			}(), animated: true)
		})
	}
	
	private func buildHeader(_ section: TableSection) -> TableSection {
		
		section.setHeaderFont(UIFont.systemFont(ofSize: 14.0, weight: .black))
		section.setHeaderTextColor(UIColor(hex: "737576")!)
		section.setHeaderColor(UIColor(hex: "212121")!)
		section.setHeaderHeight(30)
		section.setHeaderIndent(15.0)

		return section
	}
	
	@IBAction func dropdownClicked(_ sender: Any?) {
		HapticUtils.SELECTION.selectionChanged()
		self.customPresentViewController(self.popupPresentr, viewController: (self.storyboard?.instantiateViewController(withIdentifier: "popup-view"))!, animated: true)
	}
	
	func nameChangedFetch() {
		HapticUtils.SELECTION.selectionChanged()
		
		self.loading = true
		self.userData = nil
		
		self.reloadTable()
		
		self.loadingIndicator.isHidden = false
		self.loadingIndicator.startAnimating()
		
		self.tableView.refreshControl = nil
		
		self.fetchData(force: true) {
			if self.userData == nil {
				HapticUtils.IMPACT.impactOccurred()
			} else {
				HapticUtils.SELECTION.selectionChanged()
			}
			
			self.loadingIndicator.stopAnimating()
			self.reloadTable()
			
			self.setupRefresh()
		}
	}
	
	@objc private func pullFetchData() {
		HapticUtils.SELECTION.selectionChanged()
		self.fetchData(force: true) {
			if self.userData == nil {
				HapticUtils.IMPACT.impactOccurred()
			} else {
				HapticUtils.SELECTION.selectionChanged()
			}
			
			self.tableView.refreshControl?.endRefreshing()
			self.reloadTable()
		}
	}
	
	private func fetchData(force: Bool, completed: @escaping () -> Void = {}) {
		self.loading = true
		
		InvestmentManager.instance.fetchData(InvestmentManager.instance.selectedUserPreference.mainUser!, force: force) {
			data in
			
			self.loading = false
			self.userData = data
			completed()
		}
	}
	
}

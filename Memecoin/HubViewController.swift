//
//  HubViewController.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/8/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import AddictiveLib
import UIKit
import Presentr

class HubViewController: UIViewController {
	
	private let enterUserPresentr: Presentr = {
		let presentr = Presentr(presentationType: .popup)
		presentr.cornerRadius = 8
		presentr.keyboardTranslationType = .compress
		presentr.shouldIgnoreTapOutsideContext = true
		presentr.dismissOnSwipe = false
		presentr.dismissOnTap = false
		presentr.dismissTransitionType = TransitionType.crossDissolve
		return presentr
	}()
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		if InvestmentManager.instance.selectedUserPreference.mainUser == nil { // Username has already been set
			self.customPresentViewController(self.enterUserPresentr, viewController: (self.storyboard?.instantiateViewController(withIdentifier: "enter-user-view"))!, animated: true)
		} else { // No username inputted
			self.present((self.storyboard?.instantiateViewController(withIdentifier: "main-view"))!, animated: true, completion: nil)
		}
	}
	
}

//
//  RedditManager.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/10/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import AddictiveLib

class RedditManager: Manager {
	
	static let instance = RedditManager()
	
	var investmentData: [InvestmentManager.InvestmentID: SubmissionData] = [:]
	
	init() {
		super.init("Reddit")
	}
	
	func fetchData(_ investment: UserTransaction, force: Bool, callback: @escaping (SubmissionData?) -> Void) {
		if !force && self.investmentData[investment.id] != nil {
			callback(self.investmentData[investment.id])
			return
		}
		
		PostInfoWebCall(investment).callback() {
			error, data in
			self.investmentData[investment.id] = data
			
			callback(data)
		}.execute()
	}
	
}

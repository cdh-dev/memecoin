//
//  PostInfoWebCall.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/10/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import AddictiveLib
import Unbox

class PostInfoWebCall: UnboxWebCall<PostDataPayload, SubmissionData> {
	
	init(_ investment: UserTransaction) {
		super.init(url: "https://www.reddit.com/api/", call: "info.json")
		
		self.parameter("id", val: "t3_\(investment.postId),t1_\(investment.responseId)")
	}
	
	override func convertToken(_ data: PostDataPayload) -> SubmissionData? {
		if data.postResponse == nil || data.postSubmission == nil {
			return nil
		}
		
		let submission = data.postSubmission!
		let response = data.postResponse!
		
		return SubmissionData(title: submission.title, author: submission.author, birthday: Date.init(timeIntervalSince1970: TimeInterval(submission.creation)), upvotes: submission.upvotes, score: submission.score, link: URL(string: "https://www.reddit.com\(submission.link)")!, thumbnail: URL(string: submission.thumbnailLink), reponseLink: URL(string: "https://www.reddit.com\(response.link)")!, reponseBody: response.body, responseBirthday: Date.init(timeIntervalSince1970: TimeInterval(response.creation)))
	}
	
}

class PostDataPayload: WebCallPayload {
	
	var postSubmission: PostSubmissionDataPayload?
	var postResponse: PostResponseDataPayload?
	
	required init(unboxer: Unboxer) throws {
		if let data = unboxer.dictionary["data"] as? [String: Any], let children = data["children"] as? [Any] {
			for child in children {
				guard let childData = child as? [String: Any], let type = childData["kind"] as? String else {
					continue
				}
				
				let unboxer = Unboxer(dictionary: childData)
				if type == "t3" { // Post
					self.postSubmission = unboxer.unbox(key: "data")
				} else if type == "t1" { // Comment
					self.postResponse = unboxer.unbox(keyPath: "data")
				}
			}
		}
	}
	
}

class PostSubmissionDataPayload: WebCallPayload {
	
	let title: String
	let upvotes: Int
	let score: Int
	let thumbnailLink: String
	let author: String
	let link: String
	let creation: CLong
	
	required init(unboxer: Unboxer) throws {
		self.title = try unboxer.unbox(key: "title")
		self.upvotes = try unboxer.unbox(key: "ups")
		self.score = try unboxer.unbox(key: "score")
		self.thumbnailLink = try unboxer.unbox(key: "url")
		self.author = try unboxer.unbox(key: "author")
		self.link = try unboxer.unbox(key: "permalink")
		self.creation = try unboxer.unbox(key: "created_utc")
	}
	
}

class PostResponseDataPayload: WebCallPayload {
	
	let link: String
	let body: String
	let creation: CLong
	
	required init(unboxer: Unboxer) throws {
		self.link = try unboxer.unbox(key: "permalink")
		self.body = try unboxer.unbox(key: "body")
		self.creation = try unboxer.unbox(key: "created_utc")
	}
	
}

//
//  InvestmentManager.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/6/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import AddictiveLib

class InvestmentManager: Manager {
	
	static let instance = InvestmentManager()
	typealias Username = String
	typealias InvestmentID = Int

	private(set) var selectedUserPreference: SelectedUserPrefsModule!
	
	var userData: [Username: UserData]
	
	init() {
		self.userData = [:]
		
		super.init("Investment")
		
		self.selectedUserPreference = SelectedUserPrefsModule()
		self.registerStorage(self.selectedUserPreference)
	}
	
	func fetchData(_ name: Username, force: Bool, callback: @escaping (UserData?) -> Void) {
		if !force && self.userData[name] != nil {
			callback(self.userData[name])
			return
		}
		
		ProcessChain().link() {
			chain in
			
			UserDataWebCall(name: name).callback() {
				error, data in
				
				guard let result = data else {
					chain.next(false)
					return
				}
				
				chain.setData("user", data: result)
				chain.next()
			}.execute()
			
		}.link() {
			chain in
			
			let user: UserData = chain.getData("user")!
			
			UserTransactionsWebCall(name).callback() {
				error, data in
				
				if let transactions = data {
					user.transactions = transactions
				}
				
				chain.next()
			}.execute()
			
		}.link() {
			chain in
			
			DownloadImageUrlCall(name: name).callback() {
				error, data in
				
				if data != nil {
					if data != nil {
						chain.setData("image-url", data: data!)
					}
				}
				
				chain.next()
			}.execute()

		}.link() {
			chain in
			
			let user: UserData = chain.getData("user")!
			
			if chain.hasData("image-url") {
				let url: URL = chain.getData("image-url")!
				
				DownloadImageCall(url: url).callback() {
					error, data in
					
					user.image = data
					chain.next()
				}.execute()
				
			} else {
				chain.next()
			}
		}.success {
			chain in
			
			callback(chain.getData("user"))
		}.failure {
			chain in
			
			callback(nil)
		}.start()
	}
	
}

//
//  PredictResultWebCall.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/30/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import AddictiveLib
import Unbox

class PredictResultWebCall: UnboxWebCall<PredictionPayload, Double> {
	
	init(initial: Int, current: Int) {
		super.init(call: "calculate")
		
		self.parameter("new", val: "\(current)")
		self.parameter("old", val: "\(initial)")
	}
	
	override func convertToken(_ data: PredictionPayload) -> Double? {
		return data.factor
	}
	
}

class PredictionPayload: WebCallPayload {
	
	let factor: Double
	
	required init(unboxer: Unboxer) throws {
		self.factor = try unboxer.unbox(key: "factor")
	}
	
}

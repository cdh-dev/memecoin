//
//  InvestmentCell.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/6/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import AddictiveLib
import UIKit

class InvestmentCell: TableCell {
	
	var investment: UserTransaction
	
	init(investment: UserTransaction) {
		self.investment = investment
		
		super.init("investment")

		self.setCallback() {
			template, cell in
			
			if cell is UIInvestmentCell {
				let cell = cell as! UIInvestmentCell
				
				cell.initialInvestmentLabel.text = "\(self.investment.amount)"

				let age = TimeUtils.timeToDate(to: self.investment.time)
				cell.ageLabel.text = "\(TimeUtils.timeToString(years: age.years, days: age.days, hours: age.hours, min: age.minutes, sec: age.seconds)) ago"
			}
		}
	}
	
}

class UIInvestmentCell: UITableViewCell {

	@IBOutlet weak var initialInvestmentLabel: UILabel!
	@IBOutlet weak var ageLabel: UILabel!
	
}


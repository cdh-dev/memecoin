//
//  UserExistsWebCall.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/13/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import AddictiveLib
import Unbox

class UserExistsWebCall: WebCall<Bool> {
	
	init(username: String) {
		super.init(url: "https://www.reddit.com/", call: "user/\(username).json")
	}
	
	override func convertData(_ data: Data) -> Bool? {
		return true // Return true cuz it'll only get here if it has data and didn't throw a 404
	}
	
}

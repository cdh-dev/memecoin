//
//  UserDataWebCall.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/7/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import AddictiveLib
import Unbox

class UserDataWebCall: UnboxWebCall<UserDataPayload, UserData> {
	
	init(name: String) {		
		super.init(call: "investor/\(name)")
	}
	
	override func convertToken(_ data: UserDataPayload) -> UserData? {
		return UserData(data.name, image: nil, balance: data.balance, broke: data.broke)
	}
	
}

class UserDataPayload: WebCallPayload {
	
	let name: String
	let balance: Int64
	let broke: Int
	
	required init(unboxer: Unboxer) throws {
		self.name = try unboxer.unbox(key: "name")
		self.balance = try unboxer.unbox(key: "balance")
		self.broke = try unboxer.unbox(key: "broke")
	}
	
}



//
//  EnterNameViewController.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/8/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import AddictiveLib
import UIKit

class EnterNameViewController: UIViewController, UITextFieldDelegate {
	
	@IBOutlet weak var submitButton: UIButton!
	@IBOutlet weak var textField: UITextField!
	@IBOutlet weak var belowNameLabel: UILabel!
	
	private var belowColor: UIColor!
	private var belowTitle: String!
	
	var searching = false
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.textField.delegate = self
		
		self.textField.text = ""
		self.submitButton.isHidden = true
		
		self.belowColor = self.belowNameLabel.textColor
		self.belowTitle = self.belowNameLabel.text!
	}
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		if self.searching {
			return false
		}
		
		if let text = textField.text, let textRange = Range(range, in: text) {
			let updatedText = text.replacingCharacters(in: textRange, with: string).replacingOccurrences(of: " ", with: "")
			
			if updatedText == "" {
				self.showButton(false)
			} else {
				self.showButton(true)
			}
		}
		
		if string == " " {
			return false
		}
		
		self.belowNameLabel.textColor = self.belowColor
		self.belowNameLabel.text = self.belowTitle
		
		return true
	}
	
	private func showButton(_ bool: Bool) {
		self.submitButton.isHidden = !bool
	}
	
	@IBAction func submitClicked(_ sender: UIButton) {
		if !self.searching && self.textField.text != nil && !self.submitButton.isHidden {
			let text = self.textField.text!

			self.textField.resignFirstResponder()

			self.searching = true
			self.submitButton.setTitle("Looking...", for: .normal)
			
			HapticUtils.SELECTION.selectionChanged()

			UserExistsWebCall(username: text).callback() {
				error, data in

				if let error = error {
					var returnMessage = ""
					if error.cause == .remote, let message = error.message, message.contains("404") {
						returnMessage = "User not found"
					} else {
						returnMessage = "An unknown error occured"
					}
					
					self.searching = false
					
					HapticUtils.IMPACT.impactOccurred()
					
					self.submitButton.setTitle("Look", for: .normal)
					
					DispatchQueue.main.async{
						self.belowNameLabel.textColor = UIColor(hex: "F32323")
						self.belowNameLabel.text = returnMessage
					}
				} else {
					HapticUtils.SELECTION.selectionChanged()
					
					InvestmentManager.instance.selectedUserPreference.userChangedUserValue(text)
					self.present((self.storyboard?.instantiateViewController(withIdentifier: "main-view"))!, animated: true, completion: nil)
				}
			}.execute()
		}
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		textField.resignFirstResponder()
		return true
	}
	
	func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
		if self.searching {
			return false
		}
		return true
	}
	
}

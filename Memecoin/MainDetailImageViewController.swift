//
//  MainDetailImageViewController.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/11/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import UIKit

class MainDetailImageViewController: UIViewController {
	
	var image: UIImage!
	@IBOutlet weak var imageView: UIImageView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.imageView.image = self.image
	}
	
}

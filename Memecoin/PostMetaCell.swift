//
//  PostMetaCell.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/12/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import AddictiveLib
import UIKit

class PostMetaCell: TableCell {
	
	let name: String
	let birthday: String
	
	init(name: String, birthday: String) {
		self.name = name
		self.birthday = birthday
		
		super.init("post-meta")
		
		self.setSelectionStyle(.none)
		self.setHeight(UITableViewAutomaticDimension)
		self.setCallback() {
			template, cell in
			
			guard let cell = cell as? UIPostMetaCell else {
				return
			}
			
			cell.authorNameLabel.text = self.name
			cell.ageLabel.text = self.birthday
		}
	}
	
}

class UIPostMetaCell: UITableViewCell {
	
	@IBOutlet weak var authorNameLabel: UILabel!
	@IBOutlet weak var ageLabel: UILabel!
	
}

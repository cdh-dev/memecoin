//
//  OptionsPopupViewController.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/11/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import AddictiveLib
import UIKit
import Presentr

class OptionsPopupViewController: UIViewController {
	
	private let itemPresentr: Presentr = {
		let presentr = Presentr(presentationType: .popup)
		presentr.cornerRadius = 8
		presentr.dismissOnSwipe = true
		presentr.keyboardTranslationType = .compress
		return presentr
	}()
	
	@IBAction func changeUsernameClicked(_ sender: UIButton) {
		HapticUtils.SELECTION.selectionChanged()
		
		self.dismiss(animated: true, completion: nil)
		self.presentingViewController!.customPresentViewController(self.itemPresentr, viewController: (self.storyboard?.instantiateViewController(withIdentifier: "option-name-view"))!, animated: true)
	}
	
	@IBAction func visitRedditClicked(_ sender: UIButton) {
		HapticUtils.SELECTION.selectionChanged()

		self.dismiss(animated: true, completion: nil)
		self.openUrl("https://www.reddit.com/r/memeeconomy/")
	}
	
	@IBAction func visitUserClicked(_ sender: UIButton) {
		HapticUtils.SELECTION.selectionChanged()
		
		self.dismiss(animated: true, completion: nil)
		self.openUrl("https://www.reddit.com/u/\(InvestmentManager.instance.selectedUserPreference.mainUser ?? "")")
	}
	
	@IBAction func visitMarketClicked(_ sender: UIButton) {
		HapticUtils.SELECTION.selectionChanged()
		
		self.dismiss(animated: true, completion: nil)
		self.openUrl("https://memes.market/")
	}
	
	@IBAction func aboutClicked(_ sender: UIButton) {
		HapticUtils.SELECTION.selectionChanged()
		
		self.dismiss(animated: true, completion: nil)
	}
	
	private func openUrl(_ link: String) {
		guard let url = URL(string: link) else {
			print("Failed to build URL: \(link)")
			return
		}
		
		UIApplication.shared.open(url, options: [:]) {
			result in
			
			if !result {
				print("Failed to open URL: \(link)")
			}
		}
	}
	
}

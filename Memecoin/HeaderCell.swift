//
//  HeaderCell.swift
//  Memecoin
//
//  Created by Dylan Hanson on 6/6/18.
//  Copyright © 2018 Dylan Hanson. All rights reserved.
//

import Foundation
import UIKit
import AddictiveLib

class HeaderCell: TableCell {
	
	var userIcon: UIImage?
	var username: String
	var coins: Int64
	var bankruptcies: Int
	
	init(icon: UIImage?, username: String, coins: Int64, bankruptcies: Int) {
		self.userIcon = icon
		self.username = username
		self.coins = coins
		self.bankruptcies = bankruptcies
		
		super.init("header")
		
		self.setCallback() {
			template, cell in
			
			if cell is UIHeaderCell {
				let cell = cell as! UIHeaderCell
				
				cell.userIconImage.image = self.userIcon
				cell.usernameLabel.text = "u/\(self.username)"
				cell.coinsLabel.text = "\(self.coins) memecoins"
				cell.bankruptciesLabel.text = "\(self.bankruptcies) \(self.bankruptcies == 1 ? "bankruptcy" : "bankruptcies")"
			}
		}
	}
	
}

class UIHeaderCell: UITableViewCell {
	
	@IBOutlet weak var userIconImage: UIImageView!
	@IBOutlet weak var usernameLabel: UILabel!
	@IBOutlet weak var coinsLabel: UILabel!
	@IBOutlet weak var bankruptciesLabel: UILabel!
	
}
